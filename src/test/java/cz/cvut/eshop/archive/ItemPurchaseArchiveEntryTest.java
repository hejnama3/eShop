package cz.cvut.eshop.archive;

import org.junit.Assert;
import org.junit.Test;

public class ItemPurchaseArchiveEntryTest extends AbstractArchiveTest {

	@Test
	public void testGetCountHowManyTimesHasBeenSold() {
		Assert.assertEquals("Incorrect sold count in empty archive", 1, itemArchive.getCountHowManyTimesHasBeenSold());
	}

	@Test
	public void testGetRefItem() {
		Assert.assertEquals("Constructor and getter items are different", item, itemArchive.getRefItem());
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldZero() {
		compareIncrement(0);
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldPositive() {
		compareIncrement(10);
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldNegative() {
		compareIncrement(-10);
	}

	@Test
	public void testToString() {
		Assert.assertEquals("ITEM  "+ item.toString()+"   HAS BEEN SOLD "+ 1 +" TIMES", itemArchive.toString());
	}
	private void compareIncrement(int difference) {
		int soldCount = itemArchive.getCountHowManyTimesHasBeenSold();
		itemArchive.increaseCountHowManyTimesHasBeenSold(difference);
		Assert.assertEquals("Incorrect sold count for " + difference + " increment", soldCount + difference, itemArchive.getCountHowManyTimesHasBeenSold());
	}
}